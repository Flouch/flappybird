/*
 * Shader.hpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#ifndef SHADER_HPP_
#define SHADER_HPP_

#include "glew/glew.h"
#include "GL/glfw.h"
#include "glm/glm.hpp"

#ifdef __APPLE__
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

namespace imac {

class Shader {
protected:
	GLuint _program;
	enum ShaderType
	{
		VERTEX_SHADER = 1,
		FRAGMENT_SHADER = 2,
		GEOMETRY_SHADER = 4
	};

public:
	Shader();
	Shader(const char* filename);
	virtual ~Shader();

	int compileAndLink(int typeMask, const char * sourceBuffer, int bufferSize);
	int loadFromFile(const char * path, int typemask);

	inline GLuint getProgram() { return this->_program; };
};

} /* namespace imac */
#endif /* SHADER_HPP_ */
