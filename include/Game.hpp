/*
 * Game.hpp
 *
 *  Created on: Aug 23, 2014
 *      Author: florent
 */

#ifndef GAME_HPP_
#define GAME_HPP_

#include "boost/shared_ptr.hpp"
#include "Renderer.hpp"
#include "Cube.hpp"
#include "Plane.hpp"
#include <vector>

namespace imac {

class Game {
protected:
	std::vector<boost::shared_ptr<Drawable> > _drawables;
	Renderer& _renderer;

public:
	Game(Renderer& renderer);
	virtual ~Game();

	void init();
	void drawScene();
};

} /* namespace imac */
#endif /* GAME_HPP_ */
