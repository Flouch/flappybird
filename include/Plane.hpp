/*
 * Plane.hpp
 *
 *  Created on: Aug 23, 2014
 *      Author: florent
 */

#ifndef PLANE_HPP_
#define PLANE_HPP_

#include "Shape.hpp"

namespace imac {

class Plane : public Shape {
public:
	Plane();
	virtual ~Plane();
};

} /* namespace imac */
#endif /* PLANE_HPP_ */
