/*
 * Drawable.hpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#ifndef DRAWABLE_HPP_
#define DRAWABLE_HPP_

#include "Shape.hpp"
#include "boost/shared_ptr.hpp"

typedef unsigned int GLuint;

namespace imac {

class Drawable {
private:
	GLuint _diffuse;
	GLuint _spec;

	boost::shared_ptr<Shape> _shape;

public:
	Drawable(boost::shared_ptr<Shape> shape);
	virtual ~Drawable();

	void initTexture(GLuint diffuse = 0, GLuint specular = 1);

	inline const GLuint diffuse() { return _diffuse; };
	inline const GLuint spec() { return _spec; };

	inline boost::shared_ptr<Shape> shape() { return _shape; };
};

}

#endif /* DRAWABLE_HPP_ */
