/*
 * Renderer.hpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#ifndef RENDERER_HPP_
#define RENDERER_HPP_

#include "Camera.hpp"
#include "Shader.hpp"
#include "Drawable.hpp"

#include <vector>
#include "boost/shared_ptr.hpp"

namespace imac{

class Renderer {
private:
	Camera& _camera;
	Shader& _shader;
	std::vector<GLuint> _texturesVector;

	GLuint _projectionLocation;
	GLuint _viewLocation;
	GLuint _objectLocation;
	GLuint _timeLocation;
	GLuint _diffuseLocation;
	GLuint _specLocation;
	GLuint _intensityLocation;
	GLuint _cameraPositionLocation;

	glm::mat4 _projection;
	glm::mat4 _worldToView;
	glm::mat4 _objectToWorld;


public:
	Renderer(Shader& shader, Camera& camera);
	virtual ~Renderer();

	GLuint loadTexture(const char* filename, int colors, int req_comp);

	void reset();
	void swapBuffers();

	void drawScene(std::vector<boost::shared_ptr<Drawable> > drawables);
	void draw(boost::shared_ptr<Drawable> drawable);

	void init();
	void initTextures();
	void initUniformParams();

	int checkError(const char* f = "main", const char* file = __FILE__, int l = __LINE__);

	static const int width;
	static const int height;

	static int createWindow(const char* name);
	static void closeWindow();
};

}

void displayMatrix(glm::mat4 mat);

#endif /* RENDERER_HPP_ */
