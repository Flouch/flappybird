/*
 * GUI.hpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#ifndef GUI_HPP_
#define GUI_HPP_

namespace imac {

class GUI {
protected:
	bool _panLock;
	bool _turnLock;
	bool _zoomLock;
	int _lockPositionX;
	int _lockPositionY;
	int _camera;
	double _time;
	bool _playing;

public:
	GUI();
	virtual ~GUI();

	void init();
	void reset();

	static const float MOUSE_PAN_SPEED;
	static const float MOUSE_ZOOM_SPEED;
	static const float MOUSE_TURN_SPEED;
};

} /* namespace imac */
#endif /* GUI_HPP_ */
