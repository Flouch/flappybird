/*
 * Shape.hpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#ifndef SHAPE_HPP_
#define SHAPE_HPP_

namespace imac {

typedef unsigned int GLuint;

class Shape {
private:
	int _triangleCount;
	int* _triangleList;
	float* _uvs;
	float* _vertices;
	float* _normals;

	GLuint _vao;
	GLuint _vbo[4];

public:
	Shape();
	Shape(int count, int* triangleList, float* uvs, float* vertices, float* normals);
	virtual ~Shape();

	void init(int count, int* triangleList, float* uvs, float* vertices, float* normals);

	inline const int triangleCount() { return _triangleCount; };
	inline const GLuint vao() { return _vao; };
};

}

#endif /* SHAPE_HPP_ */
