/*
 * Cube.hpp
 *
 *  Created on: Aug 23, 2014
 *      Author: florent
 */

#ifndef CUBE_HPP_
#define CUBE_HPP_

#include "Shape.hpp"

namespace imac {

class Cube : public Shape {
public:
	Cube();
	virtual ~Cube();
};

} /* namespace imac */
#endif /* CUBE_HPP_ */
