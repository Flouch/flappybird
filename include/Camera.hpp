/*
 * camera.hpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include "glm/glm.hpp"
#include "glm/vec3.hpp" // glm::vec3

namespace imac {

class Camera {

protected:
	float _radius;
	float _theta;
	float _phi;
	glm::vec3 _o;
	glm::vec3 _eye;
	glm::vec3 _up;

public:
	Camera();
	~Camera();

	void compute();
	void defaults();
	void zoom(float factor);
	void turn(float phi, float theta);
	void pan(float x, float y);

	inline const glm::vec3 getO() { return _o; }
	inline const glm::vec3 getEye() { return _eye; }
	inline const glm::vec3 getUp() { return _up; }

};

}


#endif /* CAMERA_HPP_ */
