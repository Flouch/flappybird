/*
 * Plane.cpp
 *
 *  Created on: Aug 23, 2014
 *      Author: florent
 */

#include "../include/Plane.hpp"

namespace imac {

Plane::Plane() {
	// TODO Auto-generated constructor stub
    int plane_triangleCount = 2;
    int plane_triangleList[] = {0, 1, 2, 2, 1, 3};
    float plane_uvs[] = {0.f, 0.f, 0.f, 10.f, 10.f, 0.f, 10.f, 10.f};
    float plane_vertices[] = {-50.0, -1.0, 50.0, 50.0, -1.0, 50.0, -50.0, -1.0, -50.0, 50.0, -1.0, -50.0};
    float plane_normals[] = {0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0};

    init(plane_triangleCount, plane_triangleList, plane_uvs, plane_vertices, plane_normals);

}

Plane::~Plane() {
	// TODO Auto-generated destructor stub
}

} /* namespace imac */
