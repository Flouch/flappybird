/*
 * GUI.cpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#include "../include/GUI.hpp"

#include "GL/glfw.h"

namespace imac {

const float GUI::MOUSE_PAN_SPEED = 0.001f;
const float GUI::MOUSE_ZOOM_SPEED = 0.05f;
const float GUI::MOUSE_TURN_SPEED = 0.005f;

GUI::GUI() {
	// TODO Auto-generated constructor stub
	this->init();
}

GUI::~GUI() {
	// TODO Auto-generated destructor stub
}

void GUI::init() {
	this->_panLock = false;
	this->_turnLock = false;
	this->_zoomLock = false;
	this->_lockPositionX = 0;
	this->_lockPositionY = 0;
	this->_camera = 0;
	this->_time = 0.0;
	this->_playing = false;
}

void GUI::reset(){
	// Mouse states
	int leftButton = glfwGetMouseButton( GLFW_MOUSE_BUTTON_LEFT );
	int rightButton = glfwGetMouseButton( GLFW_MOUSE_BUTTON_RIGHT );
	int middleButton = glfwGetMouseButton( GLFW_MOUSE_BUTTON_MIDDLE );

	if( leftButton == GLFW_PRESS )
		_turnLock = true;
	else
		_turnLock = false;

	if( rightButton == GLFW_PRESS )
		_zoomLock = true;
	else
		_zoomLock = false;

	if( middleButton == GLFW_PRESS )
		_panLock = true;
	else
		_panLock = false;
}

} /* namespace imac */
