/*
 * Shape.cpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#include "../include/Shape.hpp"

#include "glew/glew.h"
#include <iostream>

namespace imac {

Shape::Shape(){

}

Shape::Shape(int count, int* triangleList, float* uvs, float* vertices, float* normals){
	this->init(count, triangleList, uvs, vertices, normals);
}

Shape::~Shape(){
	delete(_triangleList);
	delete(_uvs);
	delete(_vertices);
	delete(_normals);
}

void Shape::init(int count, int* triangleList, float* uvs, float* vertices, float* normals){
	std::cerr<<"shape"<<std::endl;
	_triangleCount = count;
	_triangleList = triangleList;
	_uvs = uvs;
	_vertices = vertices;
	_normals = normals;

	glGenVertexArrays(1, &_vao);
	glGenBuffers(4, _vbo);

	// Cube
	glBindVertexArray(_vao);
	// Bind indices and upload data
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[0]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(_triangleList), _triangleList, GL_STATIC_DRAW);
	// Bind vertices and upload data
	glBindBuffer(GL_ARRAY_BUFFER, _vbo[1]);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*3, (void*)0);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_vertices), _vertices, GL_STATIC_DRAW);
	// Bind normals and upload data
	glBindBuffer(GL_ARRAY_BUFFER, _vbo[2]);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*3, (void*)0);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_normals), _normals, GL_STATIC_DRAW);
	// Bind uv coords and upload data
	glBindBuffer(GL_ARRAY_BUFFER, _vbo[3]);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*2, (void*)0);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_uvs), _uvs, GL_STATIC_DRAW);

	// Unbind everything. Potentially illegal on some implementations
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

}

