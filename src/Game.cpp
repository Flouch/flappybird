/*
 * Game.cpp
 *
 *  Created on: Aug 23, 2014
 *      Author: florent
 */

#include "../include/Game.hpp"

namespace imac {

Game::Game(Renderer& renderer) : _renderer(renderer) {
	// TODO Auto-generated constructor stub
	init();

}

Game::~Game() {
	// TODO Auto-generated destructor stub
}

void Game::drawScene(){
	_renderer.drawScene(_drawables);
}

void Game::init() {

	// we add some shapes
	boost::shared_ptr<Shape> cube( new Cube() );
	boost::shared_ptr<Drawable> dCube( new Drawable(cube) );

	boost::shared_ptr<Shape> plane( new Plane() );
	boost::shared_ptr<Drawable> dPlane( new Drawable(plane) );

	_drawables.push_back(dCube);
	_drawables.push_back(dPlane);

}

} /* namespace imac */
