#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <iostream>

#include "../include/Game.hpp"
#include "../include/GUI.hpp"
#include "boost/shared_ptr.hpp"



#include "GL/glfw.h"

//#include "imgui/imgui.h"
//#include "imgui/imguiRenderGL3.h"


#ifndef DEBUG_PRINT
#define DEBUG_PRINT 1
#endif

#if DEBUG_PRINT == 0
#define debug_print(FORMAT, ...) ((void)0)
#else
#ifdef _MSC_VER
#define debug_print(FORMAT, ...) \
		fprintf(stderr, "%s() in %s, line %i: " FORMAT "\n", \
				__FUNCTION__, __FILE__, __LINE__, __VA_ARGS__)
#else
#define debug_print(FORMAT, ...) \
		fprintf(stderr, "%s() in %s, line %i: " FORMAT "\n", \
				__func__, __FILE__, __LINE__, __VA_ARGS__)
#endif
#endif


typedef unsigned int GLuint;

using namespace imac;


int main( int argc, char **argv )
{

	// création d'une fenetre
	Renderer::createWindow("angry bird");

	Camera camera;
	GUI gui;
	Shader shader("shader/shader.glsl");
	Renderer renderer(shader, camera);
	Game game(renderer);

//	float widthf = (float) Renderer::width, heightf = (float) Renderer::height;
	double t;
//	float fps = 0.f;



	// GUI
	float intensity = 1.0;

	do
	{
		t = glfwGetTime();

//		gui.reset();

		renderer.reset();

		game.drawScene();

		renderer.checkError();

		// Swap buffers
		renderer.swapBuffers();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey( GLFW_KEY_ESC ) != GLFW_PRESS &&
			glfwGetWindowParam( GLFW_OPENED ) );

	// Clean UI
//	imguiRenderGLDestroy();

	// Close OpenGL window and terminate GLFW
	Renderer::closeWindow();

	exit( EXIT_SUCCESS );
}







