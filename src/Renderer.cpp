/*
 * Renderer.cpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#include "../include/Renderer.hpp"
#include "glew/glew.h"
#include "GL/glfw.h"

#ifdef __APPLE__
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include "glm/gtc/matrix_transform.hpp" // glm::translate, glm::rotate, glm::scale, glm::perspective
#include "glm/gtc/type_ptr.hpp" // glm::value_ptr
#include "stb/stb_image.h"

#include <cstdio>

namespace imac {

const int Renderer::width = 1024;
const int Renderer::height = 768;

int Renderer::createWindow(const char* name) {
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		exit( EXIT_FAILURE );
	}

	// Force core profile on Mac OSX
#ifdef __APPLE__
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);
	glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
	// Open a window and create its OpenGL context
	if( !glfwOpenWindow( Renderer::width, Renderer::height, 0,0,0,0, 24, 0, GLFW_WINDOW ) )
	{
		fprintf( stderr, "Failed to open GLFW window\n" );

		glfwTerminate();
		exit( EXIT_FAILURE );
	}

	glfwSetWindowTitle( name );


	// Core profile is flagged as experimental in glew
#ifdef __APPLE__
	glewExperimental = GL_TRUE;
#endif
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		exit( EXIT_FAILURE );
	}

	// Ensure we can capture the escape key being pressed below
	glfwEnable( GLFW_STICKY_KEYS );

	// Enable vertical sync (on cards that support it)
	glfwSwapInterval( 1 );
	GLenum glerr = GL_NO_ERROR;
	return glGetError();
}


void Renderer::closeWindow() {
	glfwTerminate();
}


Renderer::Renderer(Shader& shader, Camera& camera) : _shader(shader), _camera(camera) {
	init();
}

Renderer::~Renderer() {
	// TODO Auto-generated destructor stub
}

void Renderer::init(){

	this->initTextures();
	this->initUniformParams();

	glViewport( 0, 0, width, height);
}

void Renderer::initTextures(){

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	_texturesVector.push_back(loadTexture("textures/spnza_bricks_a_diff.tga", GL_RGB ,3));
	_texturesVector.push_back(loadTexture("textures/spnza_bricks_a_spec.tga", GL_RED ,1));

}

GLuint Renderer::loadTexture(const char* filename, int colors, int req_comp){
	GLuint texture;
	glGenTextures(1, &texture);
	int x;
	int y;
	int comp;
	unsigned char * tex = stbi_load(filename, &x, &y, &comp, req_comp);

	glActiveTexture(GL_TEXTURE0 + _texturesVector.size());
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, colors, x, y, 0, colors, GL_UNSIGNED_BYTE, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	fprintf(stderr, "Texture %dx%d:%d\n", x, y, comp);

	return texture;
}

void Renderer::initUniformParams(){

	GLuint program = _shader.getProgram();
	glUseProgram(program);

	_projectionLocation = glGetUniformLocation(program, "Projection");
	checkError(__FUNCTION__, __FILE__, __LINE__);
	_viewLocation = glGetUniformLocation(program, "View");
	checkError(__FUNCTION__, __FILE__, __LINE__);
	_objectLocation = glGetUniformLocation(program, "Object");
	checkError(__FUNCTION__, __FILE__, __LINE__);
	_timeLocation = glGetUniformLocation(program, "Time");
	checkError(__FUNCTION__, __FILE__, __LINE__);
	_diffuseLocation = glGetUniformLocation(program, "Diffuse");
	checkError(__FUNCTION__, __FILE__, __LINE__);
	_specLocation = glGetUniformLocation(program, "Spec");
	checkError(__FUNCTION__, __FILE__, __LINE__);
	_intensityLocation = glGetUniformLocation(program, "Intensity");
	checkError(__FUNCTION__, __FILE__, __LINE__);
	_cameraPositionLocation = glGetUniformLocation(program, "CameraPosition");
	checkError(__FUNCTION__, __FILE__, __LINE__);

	//ERROR HERE : -1 => error
	std::cerr<<(int) _projectionLocation<<", "<<(int) _viewLocation<<", "<<(int) _objectLocation<<", "<<(int) _timeLocation<<", "<<(int) _diffuseLocation<<", "<<(int) _specLocation<<", "<<(int) _intensityLocation<<", "<<(int) _cameraPositionLocation<<std::endl;
}

void Renderer::reset(){

	glEnable(GL_DEPTH_TEST);
	// Get camera matrices
	_projection = glm::perspective(45.0f, static_cast<float>(Renderer::width) / static_cast<float>(Renderer::height), 0.1f, 100.f);
	_worldToView = glm::lookAt(_camera.getEye(), _camera.getO(), _camera.getUp());
	_objectToWorld = glm::mat4();
	_camera.compute();

	// Clear the front buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


void Renderer::swapBuffers(){
	glfwSwapBuffers();
}

void Renderer::drawScene(std::vector<boost::shared_ptr<Drawable> > drawables){

	for(auto &i : drawables ){
		draw(i);
	}

}

void Renderer::draw(boost::shared_ptr<Drawable> drawable){

	// Bind textures
	checkError(__FUNCTION__, __FILE__, __LINE__);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _texturesVector[drawable->diffuse()]);
	checkError(__FUNCTION__, __FILE__, __LINE__);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _texturesVector[drawable->spec()]);
	checkError(__FUNCTION__, __FILE__, __LINE__);

	glUseProgram(_shader.getProgram());
	//	std::cerr<< "VAO used : " << drawable->shape()->vao() << std::endl;
	//	std::cerr<< "triangle count : " << drawable->shape()->triangleCount() << std::endl;
	//	std::cerr<< "Program used : " << _shader.getProgram() << std::endl;

	// Upload uniforms
	//	displayMatrix(_projection);
	glUniformMatrix4fv(_projectionLocation, 1, 0, glm::value_ptr(_projection));
	checkError(__FUNCTION__, __FILE__, __LINE__);
	//	displayMatrix(_worldToView);
	glUniformMatrix4fv(_viewLocation, 1, 0, glm::value_ptr(_worldToView));
	checkError(__FUNCTION__, __FILE__, __LINE__);
	//	displayMatrix(_objectToWorld);
	glUniformMatrix4fv(_objectLocation, 1, 0, glm::value_ptr(_objectToWorld));
	checkError(__FUNCTION__, __FILE__, __LINE__);
	glUniform1f(_timeLocation, 1.f);
	//	std::cerr << "projection : " << _camera.getEye()[0] << ", " << _camera.getEye()[2] << std::endl;
	checkError(__FUNCTION__, __FILE__, __LINE__);
	glUniform3fv(_cameraPositionLocation, 1, glm::value_ptr(_camera.getEye()));
	checkError(__FUNCTION__, __FILE__, __LINE__);
	glUniform1f(_intensityLocation, 1.0f);
	checkError(__FUNCTION__, __FILE__, __LINE__);
	glUniform1i(_diffuseLocation, 0);
	checkError(__FUNCTION__, __FILE__, __LINE__);
	glUniform1i(_specLocation, 0);
	checkError(__FUNCTION__, __FILE__, __LINE__);

	// Render vaos
	//	glBindVertexArray(drawable.vao());
	//	glDrawElementsInstanced(GL_TRIANGLES, cube_triangleCount * 3, GL_UNSIGNED_INT, (void*)0, 4);

	glBindVertexArray(drawable->shape()->vao());
	glDrawElements(GL_TRIANGLES, drawable->shape()->triangleCount() * 3, GL_UNSIGNED_INT, (void*)0);
	checkError(__FUNCTION__, __FILE__, __LINE__);

}

int Renderer::checkError(const char* f, const char* file, int l) {
	// Check for errors
	GLenum err = GL_NO_ERROR;
	err = glGetError();
	if(err != GL_NO_ERROR)
	{
		fprintf(stderr, "OpenGL Error on file %s - function %s - line %i : %s\n", file, f, l, gluErrorString(err));
		exit(1);
	}
	return 0;
}

}

void displayMatrix(glm::mat4 mat){
	std::cerr << "Matrix : {" << std::endl;
	for(int i=0 ; i < 4; ++i) {
		for(int j=0; j < 4; ++j){
			std::cerr << mat[j][i] << ", ";
		}
		std::cerr << std::endl;
	}
	std::cerr << "}" << std::endl;
}
