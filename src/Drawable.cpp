/*
 * Drawable.cpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#include "../include/Drawable.hpp"

#include <iostream>

namespace imac {

Drawable::Drawable(boost::shared_ptr<Shape> shape) {
	// TODO Auto-generated constructor stub
	_shape = shape;
	initTexture();
}

Drawable::~Drawable() {
	// TODO Auto-generated destructor stub
}

void Drawable::initTexture(GLuint diffuse, GLuint specular){
	_diffuse = diffuse;
	_spec = specular;
}

}

