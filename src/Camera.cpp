/*
 * camera.cpp
 *
 *  Created on: Aug 20, 2014
 *      Author: florent
 */

#include "../include/Camera.hpp"
#include <cmath>
#include <iostream>

namespace imac {

void Camera::compute()
{
	_eye.x = cos(_theta) * sin(_phi) * _radius + _o.x;
	_eye.y = cos(_phi) * _radius + _o.y ;
	_eye.z = sin(_theta) * sin(_phi) * _radius + _o.z;
	_up = glm::vec3(0.f, _phi < M_PI ?1.f:-1.f, 0.f);
}

void Camera::defaults()
{
	_phi = 3.14/2.f;
	_theta = 3.14/2.f;
	_radius = 10.f;
	compute();
}

void Camera::zoom(float factor)
{
	_radius += factor * _radius ;
	if (_radius < 0.1)
	{
		_radius = 10.f;
		_o = _eye + glm::normalize(_o - _eye) * _radius;
	}
	compute();
}

void Camera::turn(float phi, float theta)
{
	_theta += 1.f * theta;
	_phi   -= 1.f * phi;
	if (_phi >= (2 * M_PI) - 0.1 )
		_phi = 0.00001;
	else if (_phi <= 0 )
		_phi = 2 * M_PI - 0.1;
	compute();
}

void Camera::pan(float x, float y)
{
	glm::vec3 up(0.f, _phi < M_PI ?1.f:-1.f, 0.f);
	glm::vec3 fwd = glm::normalize(_o - _eye);
	glm::vec3 side = glm::normalize(glm::cross(fwd, up));
	_up = glm::normalize(glm::cross(side, fwd));
	_o[0] += up[0] * y * _radius * 2;
	_o[1] += up[1] * y * _radius * 2;
	_o[2] += up[2] * y * _radius * 2;
	_o[0] -= side[0] * x * _radius * 2;
	_o[1] -= side[1] * x * _radius * 2;
	_o[2] -= side[2] * x * _radius * 2;
	compute();
}

Camera::Camera(){
	defaults();
}

Camera::~Camera(){

}

}



